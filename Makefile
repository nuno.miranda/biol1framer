# all our targets are phony (no files to check).
.PHONY:

all: biol1framer

PYTHON = python3
PIP = pip3


dev:
	$(PYTHON) setup.py sdist bdist_wheel
	$(PIP) install -e . 

biol1framer:
	$(PYTHON) setup.py sdist bdist_wheel
	$(PIP) install --force-reinstall  dist/biol1framer-0.1-py3-none-any.whl

uninstall:
	$(PIP) uninstall biol1framer

clean:
	rm -rf dist build src/*egg-info
