import numpy as np
from scipy.constants import day as DAY
from datetime import datetime
from dateutil import parser

from ..xl import (xl_model_init, xl_time_ref_init_file, xl_position_on_orbit)
from ..xl import  XL_TIME_UTC,XL_ANGLE_TYPE_TRUE_LAT_EF, XP_DER_2ND
from ..xorb import (xo_orbit_init_file, xo_osv_compute,xo_orbit_get_anx)


class PoorOrbitInterpolator():
    def __init__(self, orbit_file:str):
        self.orbit_file = orbit_file
        self._model_id = xl_model_init()
        time_id, val_time0, val_time1 = xl_time_ref_init_file(time_file=self.orbit_file)

        self._time_id = time_id
        self.t0 = val_time0.value
        self.t1 = val_time1.value

        orbit_id, val_time0, val_time1 = xo_orbit_init_file(model_id=self._model_id,\
                                                        time_id=self._time_id,\
                                                        orbit_file=self.orbit_file)
        self._orbit_id = orbit_id

    def __repr__(self):
        name = self.__class__.__name__
        domain = self.to_datetime64([self.t0 * DAY, self.t1 * DAY])
        dt = (domain[1] - domain[0]).astype('timedelta64[m]')

        return f"{name}(domain={domain}, {str(dt)})"

    def __call__(self, time):
        time = np.atleast_1d(time)
        pos = []
        vel = []
        acc = []
        for t_ in time:
            pos_, vel_, acc_ = xo_osv_compute(orbit_id=self._orbit_id, time_in=t_)
            pos.append(pos_)
            vel.append(vel_)
            acc.append(acc_)
        pos = np.asarray(pos)
        vel = np.asarray(vel)
        acc = np.asarray(acc)

        return pos, vel, acc



    def get_ops_angle(self,  time, pos=None,vel=None,acc=None):
        time = np.atleast_1d(time)
        if pos is None or vel is None or acc is None:
            pos, vel, acc = self.__call__(time)

        
        angle =[]
        for i, t_ in enumerate(time):
            arg = {'model_id' : self._model_id, 
                   'time_id':self._time_id, 
                   'angle_type' : XL_ANGLE_TYPE_TRUE_LAT_EF,
                   'time_ref' : XL_TIME_UTC,
                   'time' : t_,
                   'pos' : pos[i,:], 'vel':vel[i,:], 'acc':acc[i,:],
                   'deriv' : XP_DER_2ND}

            angle_, q, v = xl_position_on_orbit(**arg)
            angle.append(angle_.value)

        return np.asarray(angle), pos, vel, acc


    @staticmethod
    def to_mjd2000(timestamp, ref=np.datetime64('2000-01-01T00:00:00Z')):
        """
        """
        if hasattr(timestamp, '__iter__') == True:
            timestamp = np.asarray(timestamp)
            if isinstance(timestamp[0] , datetime):
                ref =  datetime(2000,1,1)
                time = [(t_-ref).total_seconds() for t_ in  timestamp]
                time = np.asarray(time)
            elif isinstance(timestamp[0] , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = (np.double(time)* constants.nano).reshape(timestamp.shape)

        else:
            if isinstance(timestamp , datetime):
                ref = datetime(2000, 1, 1)
                time = (timestamp-ref).total_seconds()
            elif isinstance(timestamp , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = np.double(time)* constants.nano

        return time

    @staticmethod
    def to_datetime64(mjd2000, ref=np.datetime64('2000-01-01T00:00:00Z')):
        """
        COnvert elaspsed seconds since the reference to datetime64

        Parameter
        ----------
        mjd2000:
            elapsed seconds since the reference
        Return
        -------
        datetime64

        """
       
        mjd2000 = np.atleast_1d(mjd2000)
        dt = (mjd2000*1e9).astype(int)
        dt = dt.astype('timedelta64[ns]')
        dt = dt.squeeze()

        return ref + dt



