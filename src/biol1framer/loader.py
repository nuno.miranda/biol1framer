import ctypes as ct
import zipfile
from importlib import resources
from tempfile import TemporaryDirectory


from . import EOCFI as EOCFI_LIB_PATH

EOCFI_ZIP_FILE = 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY.zip'
EOCFI_ZIP =''
with resources.path(EOCFI_LIB_PATH, EOCFI_ZIP_FILE) as eocfi_zip: 
    EOCFI_ZIP = zipfile.ZipFile(eocfi_zip)

eolib = {'libproj' :'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libproj.so',
         'libgeotiff' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libgeotiff.so',
         'libcommon': 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiEECommon.so',
         'libfile' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiFileHandling.so',
         'libdh' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiDataHandling.so',
         'libcfi' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiLib.so', 
         'liborb' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiOrbit.so',
         'libpointing' : 'EOCFI-4.20-CPPLIB-LINUX64_LEGACY/libraries/LINUX64_LEGACY/libCfiPointing.so'
         }


EOCFI_LIBS={}
for libid, lib_path in eolib.items():
    with TemporaryDirectory() as tmpdirname:
        lib_ = EOCFI_ZIP.extract(lib_path, path=tmpdirname)
        EOCFI_LIBS[libid] = ct.CDLL(lib_)
