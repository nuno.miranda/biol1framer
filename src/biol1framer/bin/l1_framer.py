#!/usr/bin/env python
import argparse
from pathlib import Path
from datetime import datetime
from dateutil import parser as time_parser
from biol1framer import PoorOrbitInterpolator
from biol1framer import BiomassL1Framer

help_string="""Example of use:\n
                l1_framer.py -s 2018-08-10T00:32:00.944398 2018-08-10T00:33:40.944398 -d 2018-08-10T00:30:00.944398 2018-08-10T00:40:40.944398 -p  S1A_OPER_AUX_POEORB_OPOD_20180830T120747_V20180809T225942_20180811T005942.EOF -o frame_list.json"""

if __name__ == "__main__":
    
    arg_parser = argparse.ArgumentParser(prog='l1_framer', usage='%(prog)s [options]',
                                     description='BIOMASS L1 framer',
                                     add_help=True,
                                     epilog=help_string)

    arg_parser.add_argument('-s', nargs=2, dest="l0_slice_time", help='Start and stop time of the L0 slice', type=str)
    arg_parser.add_argument('-d', nargs=2, dest="l0_mon_time", help='Start and stop time of the data-take e.g. from L0 monitoring', type=str)
    arg_parser.add_argument('-p',  nargs=1,dest='orbit_file', help='path to the orbit file factor', type=str)
    arg_parser.add_argument('-o',  nargs=1,dest='json_file', help='path to output JSON file', type=str)
    
    args = arg_parser.parse_args()

    l0_mon_time = [ time_parser.parse(t) for t in args.l0_mon_time ]
    l0_slice_time = [ time_parser.parse(t) for t in args.l0_slice_time ]
    orbit_file = Path(args.orbit_file[0])
    json_file = Path(args.json_file[0])

    
    assert l0_slice_time[0]>=l0_mon_time[0], "L0 slice start time is before data-take start "
    assert l0_slice_time[1]<=l0_mon_time[1], "L0 slice stop time is after data-take start "

    framer = BiomassL1Framer(l0_mon_time[0], l0_mon_time[1],
                             str(orbit_file), l0_slice_time[0], l0_slice_time[1])
    dur= framer()

    print(framer.df)
    
    with open(json_file, 'w') as f:
       f.write(framer.to_json())