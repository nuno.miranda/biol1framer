import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scipy.constants import day as DAY
from scipy.interpolate import interp1d 

from datetime import datetime
from dateutil import parser
import pandas as pd
import numpy as np
import json
import yaml

from ..orbit.interp import PoorOrbitInterpolator as EocfiOrbitInterpolator

NUMBER_FRAMES = 310
FRAME_ANGULAR_STEP = 360/NUMBER_FRAMES
FRAME_LIST = np.arange(1,NUMBER_FRAMES+1)
FRAME_START_ANGLE = np.arange(NUMBER_FRAMES) * FRAME_ANGULAR_STEP
FRAME_STOP_ANGLE = FRAME_START_ANGLE + FRAME_ANGULAR_STEP
MIN_SLICE_DURATION = 28
MIN_FRAME_DURATION = 8

class BiomassL1Framer:
    def __init__(self, dt_start:datetime, dt_stop:datetime,
                       orbit_file:str,
                       slice_start:datetime=None,
                       slice_stop:datetime=None, ):
        """Class to compute the BIOMASS frame parameters out of L0 slice"""

        self.dt_start = dt_start
        self.dt_stop = dt_stop
     
        self.orbit_file = orbit_file

        #Initialise the EOCFI orbit interpolator to perform the slicing based on the OPS angle
        self._orbit = EocfiOrbitInterpolator(self.orbit_file)

        self._dt_start_mjd = self._orbit.to_mjd2000(self.dt_start) / DAY
        self._dt_stop_mjd = self._orbit.to_mjd2000(self.dt_stop) / DAY

        self._slice_start_mjd = self._dt_start_mjd if slice_start is None else self._orbit.to_mjd2000(slice_start) / DAY
        self._slice_stop_mjd =  self._dt_stop_mjd if slice_stop is None else self._orbit.to_mjd2000(slice_stop) / DAY

        self.__post_init__()
   
    def __post_init__(self):
        #compute the LUT from time <-> ops_angle
        times = np.arange(self._dt_start_mjd,self._dt_stop_mjd+1/DAY, 1/DAY)
        angle, pos, vel, acc = self._orbit.get_ops_angle(times)
        
        self.anx_crossing_flag = True if angle[-1] < angle[0] else False

        angle = np.unwrap(angle, period=360)
        self._time2ops = interp1d(times, angle, bounds_error=False, fill_value="extrapolate", )
        self._ops2time = interp1d(angle, times, bounds_error=False, fill_value="extrapolate")
    
    @property
    def slice_stop(self)->np.datetime64:
        return self._orbit.to_datetime64(self._slice_stop_mjd * DAY)

    @property
    def slice_start(self) -> np.datetime64:
        return self._orbit.to_datetime64(self._slice_start_mjd * DAY)

    def __frame_start_clean_up(self):
        td = (self.df.FRAME_STOP_TIME.values - self.slice_start).astype('timedelta64[ms]')/ np.timedelta64(1, 's')
        ix = np.where(np.abs(td) < MIN_FRAME_DURATION)[0]
        if ix.size > 0:
            ix = ix.item(0)
            self.df.loc[ix+1,'FRAME_START_TIME'] = self.slice_start
            dur = self.df.loc[ix+1,'FRAME_STOP_TIME'] -   self.df.loc[ix+1,'FRAME_START_TIME']
            self.df.loc[ix+1,'DURATION'] = dur.total_seconds()
            self.df.drop(ix, inplace=True)
            self.df.reset_index(drop=True, inplace=True)

    def __frame_stop_clean_up(self):
        td = (self.slice_stop - self.df.FRAME_START_TIME.values).astype('timedelta64[ms]')/ np.timedelta64(1, 's')
        ix =np.where(np.abs(td) < MIN_FRAME_DURATION)[0]
        if ix.size > 0:
            ix = ix.item(0)
    
            self.df.loc[ix-1,'FRAME_STOP_TIME'] = self.slice_stop
            dur = self.df.loc[ix-1,'FRAME_STOP_TIME'] -   self.df.loc[ix-1,'FRAME_START_TIME']
            self.df.loc[ix-1,'DURATION'] = dur.total_seconds()
            self.df.drop(ix, inplace=True)
            self.df.reset_index(drop=True)
    


    def __call__(self,):
        
        # compute the OPS angles bracketing the data-take
        ops_angles = np.asarray([self._ops2time.x[0], self._ops2time.x[-1]])
        # get start and stop frame indexes
        frame_start_stop = np.asarray((ops_angles % 360) // FRAME_ANGULAR_STEP, dtype=int)

        # if Data Take doesn't cross ANX the frame list is monotonic
        if self.anx_crossing_flag is False:
            frame_list = np.arange(FRAME_LIST[frame_start_stop[0]], FRAME_LIST[frame_start_stop[1]]+1)
        else:
            # if Data Take crosses ANX the frame list passes through NUMBER_FRAMES, then it continues from 1 on
            frame_list = np.arange(FRAME_LIST[frame_start_stop[0]], NUMBER_FRAMES+1)
            frame_list = np.concatenate((frame_list, np.arange(1, FRAME_LIST[frame_start_stop[1]]+1)), axis=None)
        # frame index in the array is exatcly equal to the frame index-1 
        # (frames goes from 1 to NUMBER_FRAMES while array index goes from 0 to NUMBER_FRAMES-1)
        frame_list_array_index = frame_list-1
        
        # Ops angle start and stop (wrapped to 360 degrees)
        ops_start_wrapped = FRAME_START_ANGLE[frame_list_array_index]
        ops_stop_wrapped = FRAME_STOP_ANGLE[frame_list_array_index]
        
        # Ops angles used for time computation are equal to the wrapped version if Data Take doesn't cross ANX
        if self.anx_crossing_flag is False:
            ops_start = ops_start_wrapped
            ops_stop = ops_stop_wrapped
        else:
            # Else OPS angles shall be unwrapped in order to provide correct monotonic times
            ops_start = np.unwrap(ops_start_wrapped, period=360)
            ops_stop = np.unwrap(ops_stop_wrapped, period=360)
        
        #compute the timing associated to each frame within the data-take
        # clipping to the actual L0M dimension
        frame_start_time = np.clip(self._orbit.to_datetime64 (self._ops2time(ops_start)*DAY),
                                     np.datetime64(self.dt_start),
                                     np.datetime64(self.dt_stop))
        frame_stop_time = self._orbit.to_datetime64 (self._ops2time(ops_stop)*DAY + 2)
        frame_stop_time = np.clip(frame_stop_time, np.datetime64(self.dt_start), np.datetime64(self.dt_stop))
        
        intervals=[]
        for left, right in zip (frame_start_time,frame_stop_time):
            intervals.append(pd.Interval(pd.Timestamp(left), pd.Timestamp(right) ,closed='left'))
        intervals= pd.arrays.IntervalArray(intervals)

        duration =  (frame_stop_time - frame_start_time).astype('timedelta64[ms]').tolist()
        duration = np.asarray([ dt.total_seconds() for dt in duration] )
        dd = {'FRAME' : frame_list, 'FRAME_START_ANGLE':ops_start_wrapped,
              'FRAME_START_TIME' : frame_start_time,
              'FRAME_STOP_TIME' : frame_stop_time,
              'DURATION' : duration, 'INTERVAL':intervals }
        # the data frame contains all the valid frames within the data-take
        self.df = pd.DataFrame.from_dict(dd)
        
        # From now on workin on the L0 Slice
        # Flag the frames belonging to the L0 slice
        slice_interval = pd.Interval(left=pd.Timestamp(self.slice_start), right=pd.Timestamp(self.slice_stop), closed='both')
        ix=[]
        for slice_ in self.df.INTERVAL:
            flag = True if slice_interval.overlaps(slice_) else False
            ix.append(flag)
        self.df['FRAME_IN_SLICE'] = ix

        #final cleanup to merge short frames
        self.__frame_start_clean_up()
        self.__frame_stop_clean_up()

        return self.df

    def to_dict(self, frame_in_slice=True):
        df = self.df[['FRAME','FRAME_START_ANGLE', 'FRAME_START_TIME', 'FRAME_STOP_TIME', 'DURATION', 'FRAME_IN_SLICE']]
        if frame_in_slice:
            df = df [df.FRAME_IN_SLICE == True]
        dd =df.to_dict(orient='list')
        
        # Make sure the frames are wrapped
        dd['FRAME'] = [f % 360 for f in dd['FRAME']]
        #convert the times
        dd['FRAME_START_TIME'] = [t.isoformat() for t in dd['FRAME_START_TIME']]
        dd['FRAME_STOP_TIME'] = [t.isoformat() for t in dd['FRAME_STOP_TIME']]
        return dd

    def to_yaml(self):
        return yaml.dump(self.to_dict())


    def to_json(self):
        return  json.dumps(self.to_dict(), indent=4) 

        
    def __repr__(self):
        name = self.__class__.__name__      
        domain = [self.dt_start , self.dt_stop]
        dt = (domain[1] - domain[0])

        return f"{name}(domain=[{self.dt_start}, {self.dt_stop}], {str(dt)})"


    def plot(self, ax=None):
        plt.style.use('ggplot')
        myFmt = mdates.DateFormatter('%H:%M:%S')
       
        imin = 999
        df  = self.df
        t0 = self.dt_start 
        t1 = self.dt_stop
       
        frame_in = df.FRAME.max()
        if ax is None:
            fig, ax = plt.subplots(nrows=1, figsize=(10, 4), )
            # plot the data-take
            ax.plot([t0, t1], [frame_in+2, frame_in+2], linewidth=4, color='black', label='Data-Take')
       
        # plot the grid
        for i, rec in df.iterrows():
            frame_start = rec.FRAME_START_TIME
            frame_stop = rec.FRAME_STOP_TIME
            frame = rec.FRAME
            if rec.FRAME_IN_SLICE:
                frame_in = frame
                ax.plot([frame_start, frame_stop], [frame, frame], marker='o')
            ax.axvline(frame_start, 0, 1, linestyle='--', linewidth=1, color='gray')
        
        # plot the slice

        slice_start = self._orbit.to_datetime64(self._slice_start_mjd * DAY)
        slice_stop = self._orbit.to_datetime64(self._slice_stop_mjd * DAY)
        ax.plot([slice_start, slice_stop], [frame_in+1, frame_in+1], linewidth=4, color='red', label='L0 Slice')

        ax.xaxis.set_major_formatter(myFmt)
        ax.set_xlabel("Data-take sensing time")
        ax.set_ylabel("L1 frame number ")
        ax.legend(loc='best')

       # plt.show()
     
        return ax

        
            