# BIOFRAMER

BIOFRAMER is a specific tool developped to compute the L1 frames included in a BIOMASS L0 Slice product

# Supported missions
This is a tool specific for BIOMASS

# Installation instructions
To use this tool, you will need:

 - A Unix-based operating system (e.g. Linux).

 - Python version 3.7 


```
git clone https://gitlab.com/nuno.miranda/biol1framer.git
cd biol1framer
conda env create -f conda_env.yml 
conda activate biol1framer
#install the pip package
make
```


