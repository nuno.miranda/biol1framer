from setuptools import setup, find_packages

setup(name='biol1framer',
      version='0.1',
      description='BIOMASS utils for estimating the L1 frame',
      url='https://gitlab.com/nuno.miranda/',
      author='ESA BIOMASS PDGS team',
      author_email='nuno.miranda@esa.int',
      license='MIT',
      packages=find_packages(where='src'),
      include_package_data=True,
      package_dir={"": "src"},
      package_data={'': ['EOCFI/*.zip', 'data/*.EOF']},
      scripts=['src/biol1framer/bin/l1_framer.py'],
      zip_safe=False,
      python_requires='==3.7.*',
      classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Programming Language :: Python :: 3'
      ]
)
